package ru.rt;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;

public class SingleArray<T> implements IArray<T>{
    public T[] array;

    public SingleArray() {
        this.array = (T[])new Object[0];
    }


    @Override
    public int size() {
        return array.length;
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public void add(T item) {
        resize();
        array[size()-1]=item;
    }

    @Override
    public void add(T item, int index) {
        T[] nArr = (T[])new Object[size()+1];
        if(!isEmpty()){
            for (int i = 0; i < size(); i++) {
                if(i>=index){
                    nArr[i+1]=array[i];
                }else if(i != index) {
                    nArr[i]=array[i];
                }
            }
        }
        nArr[index]=item;
        array = nArr;
    }

    public T put(T item, int index) {
        T val = array[index];
        array[index] = item;
        return val;
    }

    public void sort(){
        SingleArray<T> sort = new HeapSort<>(this).sort();
        array = sort.array;
    }

    @Override
    public T remove(int index) {
        if(size() == 0){
            return null;
        }
        T[] nArr = (T[])new Object[size()-1];
        T removed = array[index];
        if(!isEmpty()){
            for (int i = 0; i < index; i++) {
                nArr[i]=array[i];
            }
            for (int i = index; i < size()-1; i++) {
                nArr[i]=array[i+1];
            }
        }
        array = nArr;
        return removed;
    }

    private void resize() {
        T[] nArr = (T[])new Object[size()+1];
        if(!isEmpty()){
            System.arraycopy(array,0,nArr,0,size());
        }
        array = nArr;
    }

    @Override
    public T get(int index) {
        return array[index];
    }

    public boolean contains(T v){
        for (int i = 0; i < array.length; i++) {
            if(array[i].equals(v))return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "SingleArray{" +
                "array=[\n   " + Arrays.stream(array).map(Objects::toString).collect(Collectors.joining("\n   ")) +
                "\n]}";
    }
}
