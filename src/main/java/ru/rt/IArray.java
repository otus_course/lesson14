package ru.rt;

public interface IArray<T> {
    int size();
    boolean isEmpty();
    T get(int index);
    void add(T item);
    void add(T item, int index);
    T put(T item, int index);
    T remove(int index);
}
