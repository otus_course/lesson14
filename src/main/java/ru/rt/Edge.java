package ru.rt;

import java.util.Objects;

public class Edge implements Comparable{

    public int v1;
    public int v2;

    public int weight;

    public Edge(int v1,int v2, int weight) {
        this.v1 = v1;
        this.v2 = v2;
        this.weight = weight;
    }


    @Override
    public int compareTo(Object o) {
        if(this.weight > ((Edge)o).weight){
            return 1;
        }
        if(this.weight < ((Edge)o).weight){
            return -1;
        }
        return 0;
    }

    @Override
    public String toString() {
        return "WeightEdge{" +
                "v1=" + v1 +
                ", v2=" + v2 +
                ", weight=" + weight +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Edge edge = (Edge) o;
        return v1 == edge.v1 && v2 == edge.v2 && weight == edge.weight;
    }

    @Override
    public int hashCode() {
        return Objects.hash(v1, v2, weight);
    }
}
