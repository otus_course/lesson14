package ru.rt;

import java.util.Arrays;

public class Main {


    public static void main(String[] args) throws Exception {
        int[][] graph = new int[][]
                {
                       {0,2,3,6,0,0,0},
                       {2,0,4,0,9,0,0},
                       {3,4,0,1,7,6,0},
                       {6,0,1,0,0,4,0},
                       {0,9,7,0,0,1,5},
                       {0,0,6,4,1,0,8},
                       {0,0,0,0,5,8,0}

                };
        Deikstra deikstra = new Deikstra(graph);
        System.out.println(Arrays.deepToString(deikstra.calc(0)));
    }

}
