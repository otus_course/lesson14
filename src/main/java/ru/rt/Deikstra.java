package ru.rt;

public class Deikstra {
    private int [][] table;
    private SingleArray<Edge> result = new SingleArray<>();
    private Point[] points;
    private SingleArray<Integer> used;

    public Deikstra(int [][] table){
        this.table = table;
        this.points = new Point[table.length];

        for (int i = 0; i < points.length; i++) {
            points[i] = new Point(0,Integer.MAX_VALUE);
        }

        this.used = new SingleArray();
    }

    public Edge[] calc(int v){
        points[v]=new Point(v,0);
        recursiveCalc(v);
        Integer last = used.get(used.size() - 1);
        restorePath(last,v);

        Edge[] edges = new Edge[result.size()];
        for (int i = 0; i < result.size(); i++) {
            edges[i] = result.get(i);
        }
        return edges;
    }

    private void restorePath(Integer last, int v) {
        if(last.intValue() == v)return;
        int v1 = points[last].v;
        result.add(new Edge(v1,last, table[v1][last]),0);
        restorePath(v1,v);
    }

    private void recursiveCalc(int v) {
        if(used.contains(v))return;

        int[] weights = table[v];
        SingleArray<Edge> edges = new SingleArray();
        for (int i = 0; i < weights.length; i++) {
            int weight = weights[i];
            if(weight > 0){
                edges.add(new Edge(v,i,weight));
            }
        }
        edges.sort();
        for (int i = 0; i < edges.size(); i++) {
            Edge e = edges.get(i);
            Point top = points[e.v2];
            if(top.weight != Integer.MAX_VALUE){
                int weight = points[v].weight + e.weight;
                if(top.weight > weight){
                    points[e.v2] = new Point(e.v1, weight);
                }
            }else {
                points[e.v2] = new Point(e.v1, points[v].weight+e.weight);
            }
        }
        used.add(v);
        for (int i = 0; i < edges.size(); i++) {
            recursiveCalc(edges.get(i).v2);
        }
    }

}
